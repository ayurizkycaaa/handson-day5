import java.util.Random;
import java.util.Scanner;

public class GenerateVoucher {

    public static void generateVoucher() {
        int jumlah;
        System.out.print("Masukkan jumlah voucher yang ingin digenerate : ");
        try (Scanner keyboard = new Scanner(System.in)) {
            jumlah = keyboard.nextInt();
        }
        for(int x=1; x<=jumlah; x++){
            System.out.println(x + ". Voucher kode yang berlaku : "+ voucher());
        }
    }

    private static String voucher(){
        String huruf = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyx0123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        int length = 10;
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(huruf.length());
            char randomChar = huruf.charAt(index);
            sb.append(randomChar);
        }
        String randomString = sb.toString();
        return randomString;
    }

}